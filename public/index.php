<?php




use app\models\User;
use app\core\Application;

require_once __DIR__ . './../vendor/autoload.php';
$dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__));
$dotenv->load();
$config=[
    'db'=>[
        'dsn'=>$_ENV['DB_DSN'],
        'user'=>$_ENV['DB_USER'],
        'password'=>$_ENV['DB_PASSWORD']
    ],
    'appName'=>$_ENV['APP_NAME']??'',
    'userClass'=> User::class,
];

$app = new Application(dirname(__DIR__) ,$config);

require_once __DIR__.'./../route/web.php';