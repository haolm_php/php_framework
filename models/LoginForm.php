<?php


namespace app\models;


use app\core\Application;
use app\core\DBModel;


class LoginForm extends DBModel
{

    public string $email='';
    public string $password='';
    public string $table='users';


    public function rules(): array
    {
        return [
            'email' => [self::RULE_REQUIRED, self::RULE_EMAIL],
            'password' => [self::RULE_REQUIRED, [self::RULE_MIN, 'min' => '6']],
        ];
    }

    public function login()
    {

        $user = LoginForm::whereArray(['email' => $this->email]);

        if (!$user) {
            $this->addError('email', 'User dose not exits with email');
            return false;
        }

        if (!password_verify($this->password, $user->password)) {
            $this->addError('email', 'Password is incorrect');
            return false;
        }

          Application::$app->login($user);

    }


}