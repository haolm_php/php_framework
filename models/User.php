<?php


namespace app\models;


use app\core\DBModel;


class User extends DBModel
{
    public string $name;
    public string $email;
    public string $password;
    public string $password_confirm;
    public string $table='users';

    protected array $fillable = [
        'name',
        'email',
        'password',
    ];


    public function save(){
        $this->password = password_hash($this->password,PASSWORD_DEFAULT);
        return parent::save();
    }


    public function rules():array
    {
        return [
            'name'=>[self::RULE_REQUIRED],
            'email'=>[self::RULE_REQUIRED,self::RULE_EMAIL,[
                self::RULE_UNIQUE,'class'=>self::class]],
            'password'=>[self::RULE_REQUIRED,[self::RULE_MIN,'min'=>'6']],
            'password_confirm'=>[self::RULE_REQUIRED,[self::RULE_MATCH,'match'=>'password']]

        ];
    }

}