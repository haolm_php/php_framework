<?php
if (!function_exists('dd')) {

    /**
     * Dump the passed variables and end the script.
     *
     * @return void
     */
    function dd() {

        foreach (func_get_args() as $x) {
            echo '<pre>';
            var_dump($x);
        }
        die;
    }
}