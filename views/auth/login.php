<div class="global-container">
    <div class="card login-form">
        <div class="card-body">
            <h3 class="card-title text-center">Log in to Admin</h3>
            <div class="card-text">
                <!--
                <div class="alert alert-danger alert-dismissible fade show" role="alert">Incorrect username or password.</div> -->
                <form action="" method="post">
                    <!-- to error: add class "has-danger" -->
                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" name="email" id="email"
                               class="form-control form-control-sm <?php echo $model->hasError('email') ? 'is-invalid' : ' ' ?>"
                               value="<?php echo $model->email ?? '' ?>">
                        <div class="invalid-feedback">
                            <?php echo $model->getFirstError('email') ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" name="password" id="password"
                               class="form-control form-control-sm  <?php echo $model->hasError('password') ? 'is-invalid' : ' ' ?>"
                               value="<?php echo $model->password ?? '' ?>">
                        <div class="invalid-feedback">
                            <?php echo $model->getFirstError('password') ?>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">Sign in</button>

                    <div class="sign-up">
                        Don't have an account? <a href="/register">Create One</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<style>

    .global-container{
        height:100vh;
        display: flex;
        align-items: center;
        justify-content: center;
        background-color: #f5f5f5;
    }

    .card-title{ font-weight:300; }

    .global-container .btn{
        font-size: 14px;
        margin-top:20px;
    }


    .login-form{
        width:330px;
        margin:20px;
    }

    .sign-up{
        text-align:center;
        padding:20px 0 0;
    }

    .alert{
        margin-bottom:-30px;
        font-size: 13px;
        margin-top:20px;
    }
</style>