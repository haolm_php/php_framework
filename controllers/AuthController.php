<?php


namespace app\controllers;


use app\core\Application;
use app\core\Controller;
use app\core\middlewares\AuthMiddleware;
use app\core\Request;
use app\core\Response;
use app\models\LoginForm;
use app\models\User;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->registerMiddleware(new authMiddleware(['profile']));
    }

    public function login(Request $request,Response $response)
    {
        $this->setLayout('auth');
        $login = new LoginForm();
        if ($request->isPost()){
            $login->loadData($request->getBody());

            if (!$login->validate()){
                return $this->view('auth/login',['model'=>$login]);
            }
            $login->login();
            $response->redirect('/');
        }

        return $this->view('auth/login',['model'=>$login]);
    }
    public function register(Request $request){

        $this->setLayout('auth');
        $user = new User();
        if ($request->isPost()){
            $user->loadData($request->getBody());
            if (!$user->validate()){
                return $this->view('auth/register',['model'=>$user]);
            }
            $user->save();
            Application::$app->session->setFlash('success','Register user success');
            Application::$app->response->redirect('/');
        }
        return $this->view('auth/register',['model'=>$user]);

    }

    public function logout( Request $request, Response $response){
        Application::$app->logout();
        $response->redirect('/');

    }

    public function profile(){

    }
}