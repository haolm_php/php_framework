<?php


namespace app\core;


class Database
{
    public \PDO  $pdo;
    public function __construct(array $config)
    {
        $dsn=$config['dsn'] ?? '';
        $user=$config['user'] ?? '';
        $password=$config['password'] ?? '';
        try {
            $this->pdo = new \PDO($dsn, $user, $password);
            // set the PDO error mode to exception
            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch(\PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }


    public function prepare($sql){
        return $this->pdo->prepare($sql);
    }

}