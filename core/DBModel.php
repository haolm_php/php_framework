<?php


namespace app\core;


abstract class DBModel extends Model
{





    public function save()
    {
        $tableName = $this->table;
        $attributes = $this->fillable;

        $params = array_map(fn($attr) => ":$attr", $attributes);
        $statement = self::prepare("INSERT INTO $tableName (" . implode(',', $attributes) . ") VALUE (" . implode(',', $params) . ")");
        foreach ($attributes as $attribute) {
            $statement->bindValue(":$attribute", $this->{$attribute});
        }

        $statement->execute();
        return true;
    }

    public function whereArray($where)
    {

        $attribute = array_keys($where);

        $tableName = $this->table;

        $sql = implode("AND ", array_map(fn($attr) => "$attr = :$attr", $attribute));

        $statement = self::prepare("SELECT * FROM $tableName Where $sql");

        foreach ($where as $key => $item) {
            $statement->bindValue(":$key", $item);
        }
        $statement->execute();
        return $statement->fetchObject(static::class);
    }

    public function prepare($qsl)
    {
        return Application::$app->db->pdo->prepare($qsl);
    }
}