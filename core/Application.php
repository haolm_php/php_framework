<?php

namespace app\core;

class Application
{

    public static string $ROOT_DIR;
    public string $userClass;
    public string $layout='app';

    public Request $request;
    public Response $response;
    public Router $router;
    public Session $session;
    public Database $db;
    public ?DBModel $user;
    public View $view;
    public static Application $app;
    public ? Controller $controller= null;

    public function __construct($rootPath, array $config)
    {
        $this->userClass = $config['userClass'];

        self::$ROOT_DIR = $rootPath;
        self::$app = $this;
        $this->request = new Request();
        $this->session = new Session();
        $this->response = new Response();
        $this->router = new Router($this->request, $this->response);
        $this->view = new View();
        $this->db = new Database($config['db']);
        $primaryValue = $this->session->get('user');

        if ($primaryValue) {
            $primaryKey = (new $this->userClass)->getKeyName();
            $this->user = (new $this->userClass)->whereArray([$primaryKey => $primaryValue]);

        } else {
            $this->user = null;
        }

    }

    public function run()
    {
        try {
            //todo
            echo $this->router->resolve();
        }catch (\Exception $e){

            echo $this->router->renderView('errors/_error',[
                'exception'=>$e
            ]);
        }
    }

    public static function isGuest()
    {
        return !self::$app->user;
    }

    /**
     * @return Controller
     */
    public function getController(): Controller
    {
        return $this->controller;
    }

    /**
     * @param Controller $controller
     * @return void
     */
    public function setController(Controller $controller): void
    {
        $this->controller = $controller;
    }

    public function login(DBModel $user)
    {

        $this->user = $user;
        $primaryKey = $user->getKeyName();
        $primaryValue = $user->{$primaryKey};

        $this->session->set('user', $primaryValue);
    }

    public function logout()
    {
        $this->user = null;
        $this->session->remove('user');
    }


}