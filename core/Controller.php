<?php

namespace app\core;


use app\core\middlewares\BaseMiddleware;

class Controller
{
    public string $layout = 'app';
    public string $actions = '';
    /**
     * @var array
     */
    protected array $middlewares = [];

    public function setLayout($layout)
    {
        $this->layout = $layout;
    }

    public function view($view, $param = [])
    {
        return Application::$app->view->renderView($view, $param);
    }

    public function registerMiddleware(BaseMiddleware $middleware)
    {
        $this->middlewares[] = $middleware;
    }

    public function getMiddlewares():array{
        return  $this->middlewares;
    }
}