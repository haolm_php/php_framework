<?php
use app\controllers\AuthController;
use app\controllers\ContractController;

$app->router->get('/', [ContractController::class, 'index']);
$app->router->get('/contract', [ContractController::class, 'index']);
$app->router->post('/contract',[ContractController::class, 'store']);
$app->router->get('/login', [AuthController::class, 'login']);
$app->router->post('/login', [AuthController::class, 'login']);
$app->router->get('/register',[AuthController::class, 'register']);
$app->router->post('/register', [AuthController::class, 'register']);
$app->router->get('/logout', [AuthController::class, 'logout']);
$app->router->get('/profile', [AuthController::class, 'profile']);
$app->run();